"""
Tests DFT solver
"""

import unittest
from nose_parameterized import parameterized

from dft.dft_solver import DFTSolver
from dft.euler_solver import EulerSolver


class TestDFT(unittest.TestCase):
    """
    Tests DFT
    """

    def setUp(self):
        """
        Prepares objects to tests
        """
        euler_solver = EulerSolver()
        self._dft_solver = DFTSolver(euler_solver)

    def test_it_computes_harmonic_for_one_sample(self):
        """
        Test that it computes for one sample
        """

        # Given
        test_sample = [1]

        # When
        harmonic_from_dft = self._dft_solver.solve(test_sample)
        expected_harmonic = [1]

        # Then
        self.assertEquals(harmonic_from_dft, expected_harmonic)

    @parameterized.expand([
        (
            [1, 2, 3, 4],
            [
                10.0 + 0j,
                -2.0 + 2j,
                -2 + 0j,
                -2 + -2j
            ]

        ),
        (
            [2, 3, 4, 5],
            [
                14 + 0j,
                -2 + 2j,
                -2 + 0j,
                -2 + -2j
            ]
        ),
        (
            [
                1 + 2j,
                2 - 4j,
                3 + 6j,
                4 - 8j,
            ],
            [
                10 - 4j,
                2 - 2j,
                -2 + 20j,
                -5.9999999999999964 - 6.000000000000004j  # python cant round it oO
            ]
        )

    ])
    def test_it_computes_harmonic_for_multi_samples(self, samples_to_test, expected_harmonics):
        """
        Test that i computes for one sample

        :param [complex] samples_to_test    : Samples to be computed
        :param [complex] expected_harmonics : Expected harmonics
        """
        # When
        harmonics_from_dft = self._dft_solver.solve(samples_to_test)

        # Then
        self.assertAlmostEquals(harmonics_from_dft[0], expected_harmonics[0], 2)
        self.assertAlmostEquals(harmonics_from_dft[1], expected_harmonics[1], 2)
        self.assertAlmostEquals(harmonics_from_dft[2], expected_harmonics[2], 2)
        self.assertAlmostEquals(harmonics_from_dft[3], expected_harmonics[3], 2)

    def test_it_can_computes_amplitude_spectres(self):
        """
        Test it can computes frequency spectres
        """

        # Given
        samples = [1, 2, 3, 4]

        # When
        dft_samples = self._dft_solver.solve(samples)
        frequency_spectres = self._dft_solver.amplitude_spectres(dft_samples)
        expected_frequency_spectres = [10, 2.828, 2, 2.828]

        # Then

        self.assertAlmostEquals(frequency_spectres[0], expected_frequency_spectres[0], 3)
        self.assertAlmostEquals(frequency_spectres[1], expected_frequency_spectres[1], 3)
        self.assertAlmostEquals(frequency_spectres[2], expected_frequency_spectres[2], 3)
        self.assertAlmostEquals(frequency_spectres[3], expected_frequency_spectres[3], 3)

    def test_it_can_computes_phase_spectres(self):
        """
        Test it can computes frequency spectres
        """

        # Given
        samples = [1, 2, 3, 4]

        # When
        dft_samples = self._dft_solver.solve(samples)
        frequency_spectres = self._dft_solver.phase_spectres(dft_samples)
        expected_frequency_spectres = [0, -0.785, 0, 0.785]

        # Then

        self.assertAlmostEquals(frequency_spectres[0], expected_frequency_spectres[0], 3)
        self.assertAlmostEquals(frequency_spectres[1], expected_frequency_spectres[1], 3)
        self.assertAlmostEquals(frequency_spectres[2], expected_frequency_spectres[2], 3)
        self.assertAlmostEquals(frequency_spectres[3], expected_frequency_spectres[3], 3)
