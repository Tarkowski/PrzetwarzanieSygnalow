"""
Tests euler solver
"""

import unittest
import math
from dft.euler_solver import EulerSolver


class TestEulerSolver(unittest.TestCase):
    """
    Tests Euler solver
    """

    def setUp(self):
        self._euler_solver = EulerSolver()

    def test_that_for_0_it_returns_1(self):
        """
        Test that for 0 input value it returns 1
        """
        # Given
        angle_to_test = 0

        # When
        solution = self._euler_solver.solve(angle_to_test)
        expected_solution = 1

        # Then
        self.assertEquals(solution, expected_solution)

    def test_that_for_pi_it_returns_minus_1(self):
        """
        Test that for PI it returns 1
        """
        # Given
        angle_to_test = math.pi

        # When
        solution = self._euler_solver.solve(angle_to_test)
        expected_solution = -1

        # Then
        self.assertAlmostEquals(solution.real, expected_solution)

    def test_that_it_computes_more_complicated_expressions(self):
        """
        Test that it can compute (4pi/6)
        """
        # Given
        angle_to_test = (4 * math.pi) / 6

        # When
        solution = self._euler_solver.solve(angle_to_test)
        expected_solution_real_part = -0.5
        expected_solution_imag_part = 0.866

        # Then
        self.assertAlmostEqual(solution.real, expected_solution_real_part, 3)
        self.assertAlmostEqual(solution.imag, expected_solution_imag_part, 3)
