"""
Test DFT service
"""
import unittest
from unittest.mock import Mock
from dft.services.dft_service import DFTService


class TestDFTService(unittest.TestCase):
    """
    Test for dft service
    """
    def setUp(self):
        """
        Setup for test
        """
        self._dft_solver = Mock()
        self.dft_service = DFTService(self._dft_solver)

    def test_that_dft_service_returns_spectres(self):
        """
        Test that dft service returns spectres
        """
        # Given
        dummy_dft = [1, 2, 3]
        dummy_amplitude_spectres = [1, 2, 3]
        dummy_phase_spectres = [1, 2, 3]
        dummy_sinusoid = [0, 0, 0]

        # When
        self._dft_solver.solve = Mock(return_value=dummy_dft)
        self._dft_solver.amplitude_spectres = Mock(return_value=dummy_amplitude_spectres)
        self._dft_solver.phase_spectres = Mock(return_value=dummy_phase_spectres)

        response = self.dft_service.solve_dft(dummy_sinusoid)
        expected_spectres = {
            'amplitude_spectres': dummy_amplitude_spectres,
            'phase_spectres': dummy_phase_spectres
        }

        self.assertEqual(response, expected_spectres)
