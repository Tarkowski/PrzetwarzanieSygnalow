"""
Test sinusoid service
"""
import unittest
from unittest.mock import Mock
from sinusoid_signal.services.sinusoid_service import SinusoidService
from sinusoid_signal.sinusoid_signal import SinusoidSignal


class TestSinusoidService(unittest.TestCase):
    """
    Test sinusoid service
    """

    def setUp(self):
        """
        Setup for tests
        """
        self._sinusoid_factory_mock = Mock()
        self._sinusoid_service = SinusoidService(self._sinusoid_factory_mock)

    def test_it_generates_sinusoid(self):
        """
        Test that service generate sinusoid
        """
        # Given
        not_important_amplitude = 0
        not_important_frequency = 0
        not_important_sampling_frequency = 0
        not_important_shift = 0
        not_important_duration = 0
        sinusoid_signal = SinusoidSignal([1, 2, 3], not_important_sampling_frequency, [1, 2, 3])

        self._sinusoid_factory_mock.generate = Mock(return_value=sinusoid_signal)

        # When
        sinusoid_from_service = self._sinusoid_service.generate_sinusoid(
            not_important_amplitude,
            not_important_frequency,
            not_important_sampling_frequency,
            not_important_shift,
            not_important_duration
        )
        expected_data = {
            "sinusoid_samples": [1, 2, 3],
            "time_samples": [1, 2, 3]
        }
        # Then
        self.assertEqual(sinusoid_from_service, expected_data)

    def test_that_service_can_adds_sinusoid(self):
        """
        Test that service add sinusoid
        """
        # Given
        not_important_amplitude = 0
        not_important_frequency = 0
        not_important_sampling_frequency = 0
        not_important_shift = 0
        not_important_duration = 0
        first_sinusoid_signal = SinusoidSignal([1, 2, 3], not_important_sampling_frequency, [1, 2, 3])
        second_sinusoid_signal = SinusoidSignal([1, 2, 3], not_important_sampling_frequency, [1, 2, 3])

        # When
        self._sinusoid_factory_mock.generate = Mock(return_value=first_sinusoid_signal)
        self._sinusoid_service.generate_sinusoid(
            not_important_amplitude,
            not_important_frequency,
            not_important_sampling_frequency,
            not_important_shift,
            not_important_duration
        )
        self._sinusoid_factory_mock.create_expanded_sinusoids = Mock(
            return_value=[first_sinusoid_signal, second_sinusoid_signal]
        )
        self._sinusoid_factory_mock.generate = Mock(return_value=second_sinusoid_signal)
        added_sinusoid_from_service = self._sinusoid_service.add_sinusoid(
            not_important_amplitude,
            not_important_frequency,
            not_important_sampling_frequency,
            not_important_shift,
            not_important_duration
        )
        expected_data = {
            "first_sinusoid_samples": first_sinusoid_signal.sinusoid_samples,
            "second_sinusoid_samples": second_sinusoid_signal.sinusoid_samples,
            "summed_sinusoid_samples": [2, 4, 6],
            "time_samples": [1, 2, 3]
        }
        # Then
        self.assertEqual(added_sinusoid_from_service, expected_data)

    def test_that_service_can_sub_two_sinusoids(self):
        """
        Test that service subs sinusoid
        """
        # Given
        not_important_amplitude = 0
        not_important_frequency = 0
        not_important_sampling_frequency = 0
        not_important_shift = 0
        not_important_duration = 0
        first_sinusoid_signal = SinusoidSignal([1, 2, 3], not_important_sampling_frequency, [1, 2, 3])
        second_sinusoid_signal = SinusoidSignal([1, 2, 3], not_important_sampling_frequency, [1, 2, 3])

        # When
        self._sinusoid_factory_mock.generate = Mock(return_value=first_sinusoid_signal)
        self._sinusoid_service.generate_sinusoid(
            not_important_amplitude,
            not_important_frequency,
            not_important_sampling_frequency,
            not_important_shift,
            not_important_duration
        )
        self._sinusoid_factory_mock.create_expanded_sinusoids = Mock(
            return_value=[first_sinusoid_signal, second_sinusoid_signal]
        )
        self._sinusoid_factory_mock.generate = Mock(return_value=second_sinusoid_signal)
        subbed_sinusoid_from_service = self._sinusoid_service.sub_sinusoid(
            not_important_amplitude,
            not_important_frequency,
            not_important_sampling_frequency,
            not_important_shift,
            not_important_duration
        )
        expected_data = {
            "first_sinusoid_samples": first_sinusoid_signal.sinusoid_samples,
            "second_sinusoid_samples": second_sinusoid_signal.sinusoid_samples,
            "subbed_sinusoid_samples": [0, 0, 0],
            "time_samples": first_sinusoid_signal.time_samples
        }
        # Then
        self.assertEqual(subbed_sinusoid_from_service, expected_data)
