"""
Tests for sinusoid factory
"""

import datetime
import unittest
from sinusoid_signal.sinusoid_factory import SinusoidFactory, TooLowSamplingFrequency
from sinusoid_signal.sinusoid_signal import SinusoidSignal


class TestSinusoidFactory(unittest.TestCase):
    """
    Tests sinusoid factory
    """

    def setUp(self):
        """
        Setup fo tests
        """
        self._sinusoid_factory = SinusoidFactory()

    def test_that_factory_raises_exception_if_sampling_frequency_is_too_low(self):
        """
        Test that factory raises exception if sampling frequency is too low
        """
        # Given
        frequency = 50
        sampling_frequency = 50
        amplitude = 'Not Important'
        phase_shift = 'Not Important'
        duration = 'Not Important'

        # When & Then
        self.assertRaises(
            TooLowSamplingFrequency,
            self._sinusoid_factory.generate,
            amplitude,
            frequency,
            sampling_frequency,
            phase_shift,
            duration
        )

    def test_that_it_generate_sinusoid(self):
        """
        Test that it generate sinusoid
        """
        # Given
        frequency = 1
        sampling_frequency = 4
        amplitude = 1
        phase_shift = 0
        duration = datetime.timedelta(seconds=1)

        # When
        sinusoid_from_factory = self._sinusoid_factory.generate(
            amplitude, frequency, sampling_frequency, phase_shift, duration
        )

        self.assertEquals(len(sinusoid_from_factory._function_values), 4)
        self.assertEqual(len(sinusoid_from_factory._time_samples), 4)
        self.assertEqual(sinusoid_from_factory._sampling_frequency, sampling_frequency)

    def test_that_it_creates_expanded_sinusoid(self):
        """
        Test that it expand sinusoid
        """
        # Given

        first_sinusoid = SinusoidSignal([1, 2, 3], 4, [0, 0.1, 0.2])
        second_sinusoid = SinusoidSignal([1, 2, 3, 4, 5], 4, [0, 0.1, 0.2, 0.3, 0.4])

        # When
        expended_sinusoids = self._sinusoid_factory.create_expanded_sinusoids(first_sinusoid, second_sinusoid)
        expected_first_sinusoid_samples = [1, 2, 3, 0, 0]

        self.assertListEqual(expended_sinusoids[0].sinusoid_samples, expected_first_sinusoid_samples)
        self.assertListEqual(expended_sinusoids[0].time_samples, second_sinusoid.time_samples)
