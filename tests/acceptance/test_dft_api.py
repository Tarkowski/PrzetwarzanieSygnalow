"""
Test DFT API
"""

import unittest
import requests
from requests import Session


class TestDFTApi(unittest.TestCase):
    """
    Test DFT Api
    """

    def test_that_main_page_returns_200(self):
        """
        Test that main page returns 200
        """
        # When
        response = requests.get('http://localhost:8080/')

        # Then
        self.assertEqual(response.status_code, 200)

    def test_that_generate_sinusoid_returns_200(self):
        """
        test that generate sinusoid returns 200 when data is valid
        """
        json = {
            'amplitude': 1,
            'frequency': 1,
            'sampling': 2,
            'shift': 1,
            'duration': 1
        }
        headers = {'X-Requested-With': 'XMLHttpRequest'}
        # Given
        response = requests.post('http://localhost:8080/generate', json=json, headers=headers)

        self.assertEqual(response.status_code, 200)

    def test_that_generate_sinusoid_returns_400_when_sampling_frequency_is_too_low(self):
        """
        test that generate sinusoid returns 400 when sampling frequency is too low
        """
        json = {
            'amplitude': 1,
            'frequency': 1,
            'sampling': 1,
            'shift': 1,
            'duration': 1
        }
        headers = {'X-Requested-With': 'XMLHttpRequest'}
        # Given
        response = requests.post('http://localhost:8080/generate', json=json, headers=headers)

        self.assertEqual(response.status_code, 400)

    def test_that_generate_sinusoid_returns_401_when_data_is_incomplete(self):
        """
        test that generate sinusoid returns 200 when data is valid
        """
        json = {
            'amplitude': 1,
            'frequency': 1,
            'sampling': '',
            'shift': 1,
            'duration': 1
        }
        headers = {'X-Requested-With': 'XMLHttpRequest'}
        # Given
        response = requests.post('http://localhost:8080/generate', json=json, headers=headers)

        self.assertEqual(response.status_code, 401)

    def test_that_add_sinusoid_returns_200(self):
        """
        test that add sinusoid returns 200 adding is complete
        """
        json = {
            'amplitude': 1,
            'frequency': 1,
            'sampling': 2,
            'shift': 1,
            'duration': 1
        }
        headers = {'X-Requested-With': 'XMLHttpRequest'}
        # Given
        requests.post('http://localhost:8080/generate', json=json, headers=headers)
        response = requests.post('http://localhost:8080/add_sinusoid', json=json, headers=headers)

        self.assertEqual(response.status_code, 200)

    def test_that_sub_sinusoid_returns_200(self):
        """
        test that add sinusoid returns 200 adding is complete
        """
        json = {
            'amplitude': 1,
            'frequency': 1,
            'sampling': 2,
            'shift': 1,
            'duration': 1
        }
        headers = {'X-Requested-With': 'XMLHttpRequest'}
        # Given
        requests.post('http://localhost:8080/generate', json=json, headers=headers)
        response = requests.post('http://localhost:8080/sub_sinusoid', json=json, headers=headers)

        self.assertEqual(response.status_code, 200)

    def test_that_dft_returns_200(self):
        """
        test that add sinusoid returns 200 adding is complete
        """
        generate_sinusoid_json = {
            'amplitude': 1,
            'frequency': 1,
            'sampling': 2,
            'shift': 1,
            'duration': 1
        }
        dft_json = {
            'sinusoid_samples': [1, 2, 3],

        }
        headers = {'X-Requested-With': 'XMLHttpRequest'}
        # Given
        requests.post('http://localhost:8080/generate', json=generate_sinusoid_json, headers=headers)
        response = requests.post('http://localhost:8080/dft', json=dft_json, headers=headers)

        self.assertEqual(response.status_code, 200)
