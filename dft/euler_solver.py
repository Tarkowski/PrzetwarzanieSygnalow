"""
Euler solver
"""

import cmath


class EulerSolver:
    """
    Solves Euler formula
    """

    @staticmethod
    def solve(angle):
        """
        Solves expression

        :param angle: Angle to compute

        :rtype: complex
        """

        solution = cmath.cos(angle) + cmath.sin(angle)*1j
        return solution
