"""
DFT bootstrap
"""
from dft.euler_solver import EulerSolver
from dft.dft_solver import DFTSolver
from dft.services.dft_service import DFTService


def create_euler_solver():
    """
    Creates Euler Solver

    :rtype: EulerSolver
    """
    return EulerSolver()


def create_dft_solver():
    """
    Creates DFT solver

    :rtype: DFTSolver
    """
    return DFTSolver(create_euler_solver())


def create_dft_service():
    """
    Creates DFT service

    :rtype: DFTService
    """
    return DFTService(create_dft_solver())
