"""
Discrete Fourier transformation solver
"""
import cmath


class DFTSolver:
    """
    Solves DFT

    :param EulerSolver euler_solver : Object that solve Euler formula
    """

    def __init__(self, euler_solver):
        self._euler_solver = euler_solver

    def solve(self, samples):
        """
        Solves DFT from list of samples

        :param [complex] samples: List of samples

        :rtype: [complex]
        """
        harmonics = []
        for sample_index in range(len(samples)):
            harmonic = self._compute_harmonic(samples, sample_index)
            harmonics.append(harmonic)

        return harmonics

    def amplitude_spectres(self, DFT_samples):
        """
        Computes amplitude spectres from samples

        :param [complex] DFT_samples: List of Samples

        :rtype: [float]
        """
        return [self._count_module(harmonic) for harmonic in DFT_samples]

    def phase_spectres(self, DFT_samples):
        """
        Computes phase spectres from samples

        :param [complex] DFT_samples: List of Samples

        :rtype: [float]
        """

        return [self._count_phase(harmonic) for harmonic in DFT_samples]

    def _compute_harmonic(self, samples, which_harmonic):
        """
        Computes single harmonic from sample list

        :param [complex] samples        : List of samples
        :param int       which_harmonic : Specifies which harmonic gonna be computed

        :rtype: complex
        """

        harmonic = 0
        for index, sample in enumerate(samples):
            angle = -2j * cmath.pi * which_harmonic * index / len(samples)
            angle *= -1j  # too delete i for euler solve
            harmonic += sample * self._euler_solver.solve(angle)
        return harmonic

    @staticmethod
    def _count_module(harmonic):
        """
        Computes module of complex

        :param complex harmonic: Complex to compute module

        :rtype: float
        """
        return cmath.sqrt(harmonic.real ** 2 + harmonic.imag ** 2).real

    @staticmethod
    def _count_phase(harmonic):
        """
        Computes module of complex

        :param complex harmonic: Complex to compute module

        :rtype: float
        """
        return cmath.atan(harmonic.imag / harmonic.real).real
