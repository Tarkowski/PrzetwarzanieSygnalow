"""
DFT service
"""


class DFTService:
    """
    DFT service

    :param DFTSolver dft_solver: DFT solver
    """

    def __init__(self, dft_solver):
        """
        Initializer
        """
        self._dft_solver = dft_solver

    def solve_dft(self, signal_samples):
        """
        Solves dft

        :param [float] signal_samples: Sampled Signal

        :rtype: dict
        """

        dft = self._dft_solver.solve(signal_samples)
        amplitude_spectres = self._dft_solver.amplitude_spectres(dft)
        phase_spectres = self._dft_solver.phase_spectres(dft)

        return {
            'amplitude_spectres': amplitude_spectres,
            'phase_spectres': phase_spectres
        }
