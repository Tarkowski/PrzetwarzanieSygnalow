"""
Sinusoid factory
"""

import math
from sinusoid_signal.sinusoid_signal import SinusoidSignal
import datetime


class SinusoidFactory:
    """
    Factory for Sinusoids
    """

    def generate(self, amplitude, frequency, sampling_frequency, phase_shift, duration):
        """
        Generates Sinusoid

        :param float     amplitude          : Amplitude
        :param float     frequency          : Frequency
        :param int       sampling_frequency : Sampling frequency
        :param float     phase_shift        : Shift of function
        :param timedelta duration           : Time of simulation(in seconds)

        :rtype: SinusoidSignal
        """

        self._sampling_frequency_check(frequency, sampling_frequency)
        time_samples = self._time_sampling(duration, sampling_frequency)
        sinusoid_samples = [
            self._function_in_time_sample(frequency, amplitude, time_sample, phase_shift)
            for time_sample in time_samples
        ]

        return SinusoidSignal(sinusoid_samples, sampling_frequency, time_samples)

    @staticmethod
    def create_expanded_sinusoids(first_sinusoid, second_sinusoid):
        """
        Expand sinusoid if other has greater length

        :param Sinusoid first_sinusoid  : First Sinusoid
        :param Sinusoid second_sinusoid : Other sinusoid

        :rtype: [Sinusoid]
        """

        new_time_samples = first_sinusoid.time_samples
        is_first_sinusoid_duration_longer = len(first_sinusoid.time_samples) > len(second_sinusoid.time_samples)
        is_second_sinusoid_duration_longer = len(first_sinusoid.time_samples) < len(second_sinusoid.time_samples)
        has_first_sinusoid_more_samples = len(first_sinusoid.sinusoid_samples) > len(second_sinusoid.sinusoid_samples)
        has_second_sinusoid_more_samples = len(first_sinusoid.sinusoid_samples) < len(second_sinusoid.sinusoid_samples)

        if is_first_sinusoid_duration_longer:
            new_time_samples = first_sinusoid.time_samples
        elif is_second_sinusoid_duration_longer:
            new_time_samples = second_sinusoid.time_samples

        if has_first_sinusoid_more_samples:
            new_sinusoid_values = second_sinusoid.sinusoid_samples + [0] * (
                len(first_sinusoid.sinusoid_samples) - len(second_sinusoid.sinusoid_samples)
            )
            expanded_sinusoid = SinusoidSignal(
                new_sinusoid_values, second_sinusoid.sampling_frequency, new_time_samples
            )
            return [first_sinusoid, expanded_sinusoid]
        
        if has_second_sinusoid_more_samples:
            new_sinusoid_values = first_sinusoid.sinusoid_samples + [0] * (
                len(second_sinusoid.sinusoid_samples) - len(first_sinusoid.sinusoid_samples)
            )
            expanded_sinusoid = SinusoidSignal(
                new_sinusoid_values, first_sinusoid.sampling_frequency, new_time_samples
            )
            return [expanded_sinusoid, second_sinusoid]
           
        return [first_sinusoid, second_sinusoid]

    @staticmethod
    def _function_in_time_sample(frequency, amplitude, time_sample, phase_shift):
        """
        Computes function in time

        :param float frequency   : Frequency
        :param float amplitude   : Amplitude
        :param float time_sample : Time (scalar)
        :param float phase_shift : Shift

        :rtype: complex
        """

        omega = 2 * math.pi * frequency
        return amplitude * math.sin(math.radians(omega * time_sample) + phase_shift)

    @staticmethod
    def _time_sampling(duration, sampling_frequency):
        """
        Makes list of time samples

        :param timedelta duration           : Time duration to be sampled
        :param float     sampling_frequency : sampling frequency

        :rtype: [float]
        """
        sample_time = datetime.timedelta(seconds=1) / sampling_frequency
        sample = datetime.timedelta(seconds=0)
        samples = []

        while sample < duration:
            samples.append(sample.total_seconds() * 60)
            sample += sample_time

        return samples

    @staticmethod
    def _sampling_frequency_check(frequency, sampling_frequency):
        """
        Checks if sampling frequency is valid

        :param float frequency          : Frequency
        :param float sampling_frequency : Sampling frequency

        :rtype: None
        """

        if sampling_frequency < 2 * frequency:
            raise TooLowSamplingFrequency


class TooLowSamplingFrequency(Exception):
    """
    Too low sampling frequency exception
    """
