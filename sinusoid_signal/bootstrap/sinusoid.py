"""
Bootstrap for sinusoid generator
"""
from sinusoid_signal.sinusoid_factory import SinusoidFactory
from sinusoid_signal.services.sinusoid_service import SinusoidService


def create_sinusoid_factory():
    """
    Creates sinusoid factory

    :rtype: SinusoidFactory
    """

    return SinusoidFactory()


def create_sinusoid_service():
    """
    Creates sinusoid service

    :rtype: SinusoidService
    """

    return SinusoidService(create_sinusoid_factory())
