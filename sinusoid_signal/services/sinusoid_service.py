"""
Sinusoid service
"""


class SinusoidService:
    """
    Sinusoid service

    :param SinusoidFactory sinusoid_factory: Sinusoid Factory
    """

    def __init__(self, sinusoid_factory):
        """
        Initializer.
        """
        self._sinusoid_factory = sinusoid_factory
        self._last_generated_sinusoid = None

    def generate_sinusoid(self, amplitude, frequency, sampling_frequency, phase_shift, duration):
        """
        Generates sinusoid


        :param str float     amplitude           :  Amplitude
        :param str float     frequency           :  frequency
        :param str float     sampling_frequency  :  sampling_frequency
        :param str float     phase_shift         :  phase_shift
        :param str date_time duration            :  duration

        :rtype: {}
        """

        sinusoid = self._sinusoid_factory.generate(amplitude, frequency, sampling_frequency, phase_shift, duration)
        self._last_generated_sinusoid = sinusoid
        return {
            'sinusoid_samples': sinusoid.sinusoid_samples,
            'time_samples': sinusoid.time_samples
        }

    def add_sinusoid(self, amplitude, frequency, sampling_frequency, phase_shift, duration):
        """

        :param str float     amplitude           :  Amplitude
        :param str float     frequency           :  frequency
        :param str float     sampling_frequency  :  sampling_frequency
        :param str float     phase_shift         :  phase_shift
        :param str date_time duration            :  duration

        :rtype: {}
        """

        sinusoid = self._sinusoid_factory.generate(amplitude, frequency, sampling_frequency, phase_shift, duration)
        last_sinusoid = self._last_generated_sinusoid
        expended_sinusoids = self._sinusoid_factory.create_expanded_sinusoids(sinusoid, last_sinusoid)
        summed_sinusoid = expended_sinusoids[0] + expended_sinusoids[1]

        self._last_generated_sinusoid = summed_sinusoid

        return {
            'first_sinusoid_samples': expended_sinusoids[0].sinusoid_samples,
            'second_sinusoid_samples': expended_sinusoids[1].sinusoid_samples,
            'summed_sinusoid_samples': summed_sinusoid.sinusoid_samples,
            'time_samples': summed_sinusoid.time_samples
        }

    def sub_sinusoid(self, amplitude, frequency, sampling_frequency, phase_shift, duration):
        """

        :param str float     amplitude           :  Amplitude
        :param str float     frequency           :  frequency
        :param str float     sampling_frequency  :  sampling_frequency
        :param str float     phase_shift         :  phase_shift
        :param str date_time duration            :  duration

        :rtype: {}
        """

        sinusoid = self._sinusoid_factory.generate(amplitude, frequency, sampling_frequency, phase_shift, duration)
        last_sinusoid = self._last_generated_sinusoid
        expanded_sinusoids = self._sinusoid_factory.create_expanded_sinusoids(sinusoid, last_sinusoid)
        subbed_sinusoid = expanded_sinusoids[0] - expanded_sinusoids[1]

        self._last_generated_sinusoid = subbed_sinusoid

        return {
            'first_sinusoid_samples': expanded_sinusoids[0].sinusoid_samples,
            'second_sinusoid_samples': expanded_sinusoids[1].sinusoid_samples,
            'subbed_sinusoid_samples': subbed_sinusoid.sinusoid_samples,
            'time_samples': subbed_sinusoid.time_samples
        }