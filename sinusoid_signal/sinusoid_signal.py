"""
Sinusoid signal
"""


class SinusoidSignal:
    """
    Sinusoid

    :param [float] sampled_function   : List of values of sinusoid in time samples
    :param float   sampling_frequency : Sampling frequency
    :param [float] time_samples       : time_samples
    """

    def __init__(self, sampled_function, sampling_frequency, time_samples):
        """
        Initializer
        """

        self._function_values = sampled_function
        self._sampling_frequency = sampling_frequency
        self._time_samples = time_samples

    @property
    def sinusoid_samples(self):
        """
        Returns sinusoid samples

        :rtype: list
        """
        return self._function_values

    @property
    def time_samples(self):
        """
        Returns time samples

        :rtype: list
        """
        return self._time_samples

    @property
    def sampling_frequency(self):
        """
        Returns time samples

        :rtype: float
        """
        return self._sampling_frequency

    def __add__(self, other):
        """
        Adds two sinusoid signals

        :param SinusoidSignal other: other sinusoid

        :rtype: SinusoidSignal
        """
        self._sinusoid_check(other)
        summed_samples = [
            first_sinusoid_sample + other_sinusoid_sample
            for first_sinusoid_sample, other_sinusoid_sample in zip(self._function_values, other._function_values)
        ]
        return SinusoidSignal(
            summed_samples,
            self._sampling_frequency,
            self._time_samples
        )

    def __sub__(self, other):
        """
        Adds two sinusoid signals

        :param SinusoidSignal other: other sinusoid

        :rtype: SinusoidSignal
        """
        self._sinusoid_check(other)
        summed_samples = [
            first_sinusoid_sample - other_sinusoid_sample
            for first_sinusoid_sample, other_sinusoid_sample in zip(self._function_values, other._function_values)
        ]
        return SinusoidSignal(
            summed_samples,
            self._sampling_frequency,
            self._time_samples
        )

    def _sinusoid_check(self, other_sinusoid):
        """
        Checks if it is possible to add other sinusoid

        :param SinusoidSignal other_sinusoid: Other sinusoid

        :rtype: None
        """

        if self._sampling_frequency != other_sinusoid._sampling_frequency:
            raise DifferentSamplingFrequency


class DifferentSamplingFrequency(Exception):
    """
    Different frequency exception
    """
