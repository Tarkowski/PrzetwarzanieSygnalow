SHELL := /bin/bash

init:
	apt-get install python3-setuptools; \
	apt-get install python3-pip; \
	pip3 install virtualenv; \
	virtualenv venv; \
	source ./venv/bin/activate; \
	pip install -r requirements/requirements.txt; \
	deactivate;

all-tests: tests-unit make-run-server-for-tests acceptance

tests-acceptance: make-run-server-for-tests acceptance

make-run-server-for-tests:
	source ./venv/bin/activate;  \
	python3 -m "api.bottle.app" & > /dev/null;\
	sleep 2;\
	deactivate;	


tests-unit:
	echo "Run unit tests with green"; \
	source ./venv/bin/activate; \
	green tests/unit/;\
	deactivate;

run-server:
	echo "Run app"; \
	source ./venv/bin/activate; \
	python3 -m "api.bottle.app";\
	deactivate;

acceptance:
	echo "Run acceptance tests with green"; \
	source ./venv/bin/activate; \
	green -s1 tests/acceptance/;\
	deactivate;\
	pkill python;	

.SILENT: all-tests tests-unit run-server tests-acceptance make-run-server-for-tests
