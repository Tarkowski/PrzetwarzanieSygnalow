import os
import bottle
from bottle import Bottle, static_file


base_path = os.path.abspath(os.path.dirname(__file__))
templates_path = os.path.join(base_path, 'templates')
bottle.TEMPLATE_PATH.insert(0, templates_path)

def set_app():
    """
    Set up bottle app

    :rtype: Bottle
    """
    app = Bottle()
    @app.get('/<filename:re:.*\.js>')
    def javascripts(filename):
        return static_file(filename, root='api/bottle/static/js/')


    @app.get('/<filename:re:.*\.css>')
    def stylesheets(filename):
        return static_file(filename, root='api/bottle/static/css/')

    @app.get('/<filename:re:.*\.map>')
    def maps(filename):
        return static_file(filename, root='api/bottle/static/css/')
    return app


