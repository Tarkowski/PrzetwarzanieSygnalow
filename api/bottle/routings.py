from api.handlers.main_page import main_page
from api.handlers.sinusoid_handler import SinusoidHandler
from api.handlers.dft_handler import DftHandler


def create_routing(bottle_app):
    """
    Creates routing for requests

    :param Bottle bottle_app: Instance of bottle application

    :rtype: None
    """
    bottle_app.route(
        path='/',
        method=['GET'],
        name='main_route',
        callback=main_page
    )

    sinusoid_handler = SinusoidHandler()

    bottle_app.route(
        path='/generate',
        method=['POST'],
        name='generate_sinusoid',
        callback=sinusoid_handler.create_sinusoid
    )

    bottle_app.route(
        path='/add_sinusoid',
        method=['POST'],
        name='add_sinusoid',
        callback=sinusoid_handler.add_sinusoid
    )

    bottle_app.route(
        path='/sub_sinusoid',
        method=['POST'],
        name='sub_sinusoid',
        callback=sinusoid_handler.sub_sinusoid
    )

    dft_handler = DftHandler()

    bottle_app.route(
        path='/dft',
        method=['POST'],
        name='dft',
        callback=dft_handler.handle
    )
