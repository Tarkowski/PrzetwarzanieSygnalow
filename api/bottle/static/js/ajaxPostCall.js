function ajaxPostCall(url, data, onSuccess){
    $.ajax({
            type: "post",
            url: url,
            contentType: "application/json",
            data: JSON.stringify(data),
            success: onSuccess,
            error: function(xhr, textStatus, errorThrown){
                alert(xhr.responseText);
            }
    })
}