var last_time_samples = null;
var last_sinusoid_samples = null;
function getDataFromForm(){
    var data = {
        amplitude: $('#amplitude').val(),
        frequency: $('#frequency').val(),
        sampling:  $('#sampling').val(),
        shift:     $('#shift').val(),
        duration:  $('#duration').val()
    };
    return data;
};
$(document.getElementById('generate-signal')).click(function() {
    var data = getDataFromForm();
    function onSuccess(responseData){
        parsedData = JSON.parse(responseData);
        trace = {
            x: parsedData.time_samples,
            y: parsedData.sinusoid_samples,
            type: 'scatter'
        };
        Plotly.newPlot('sinusoid', [trace]);
        document.getElementById('add-signal').style.display = "block";
        document.getElementById('count-dft').style.display = "block";
        document.getElementById('subtract-signal').style.display = "block";
        last_sinusoid_samples=parsedData.sinusoid_samples;
        last_time_samples=parsedData.time_samples;
    };
    ajaxPostCall('/generate', data, onSuccess);
});
$(document.getElementById('add-signal')).click(function() {
    var data = getDataFromForm();
    function onSuccess(responseData){
        parsedData = JSON.parse(responseData);
        trace1 = {
            x: parsedData.time_samples,
            y: parsedData.first_sinusoid_samples,
            type: 'scatter',
            name: 'First Sinusoid'
        };
        trace2 = {
            x: parsedData.time_samples,
            y: parsedData.second_sinusoid_samples,
            type: 'scatter',
            name: 'Second Sinusoid'
        };
        trace3 = {
            x: parsedData.time_samples,
            y: parsedData.summed_sinusoid_samples,
            type: 'scatter',
            name: 'Summed Sinusoids'
        };
        Plotly.newPlot('sinusoid', [trace1, trace2, trace3]);
        last_sinusoid_samples=parsedData.summed_sinusoid_samples;
        last_time_samples=parsedData.time_samples;
    };
    ajaxPostCall('/add_sinusoid', data, onSuccess);
});
$(document.getElementById('subtract-signal')).click(function() {
    var data = getDataFromForm();
    function onSuccess(responseData){
        parsedData = JSON.parse(responseData);
        trace1 = {
            x: parsedData.time_samples,
            y: parsedData.first_sinusoid_samples,
            type: 'scatter',
            name: 'First Sinusoid'
        };
        trace2 = {
            x: parsedData.time_samples,
            y: parsedData.second_sinusoid_samples,
            type: 'scatter',
            name: 'Second Sinusoid'
        };
        trace3 = {
            x: parsedData.time_samples,
            y: parsedData.subbed_sinusoid_samples,
            type: 'scatter',
            name: 'Subbed Sinusoids'
        };
        Plotly.newPlot('sinusoid', [trace1, trace2, trace3]);
        last_sinusoid_samples=parsedData.subbed_sinusoid_samples;
        last_time_samples=parsedData.time_samples;
    };
    ajaxPostCall('/sub_sinusoid', data, onSuccess);
});
$(document.getElementById('count-dft')).click(function() {
    var data = {
        sinusoid_samples: last_sinusoid_samples
    };
    function onSuccess(responseData){
        parsedData = JSON.parse(responseData);
        amplitude_spectrum = {
            y: parsedData.amplitude_spectres,
            type: 'scatter',
            name: 'Amplitude Spectrum'
        }
        phase_spectrum = {
            y: parsedData.phase_spectres,
            type: 'scatter',
            name: 'Phase Spectrum'
        }
        Plotly.newPlot('amplitude-spectrum', [amplitude_spectrum]);
        Plotly.newPlot('phase-spectrum', [phase_spectrum]);
    };
    ajaxPostCall('/dft', data, onSuccess);
});