"""
Bottle application
"""

from api.bottle.settings import set_app
from api.bottle.routings import create_routing

if __name__ == "__main__":
    app = set_app()
    create_routing(app)
    app.run(host='localhost', port=8080, debug=True, reload=True)
