"""
Main page handler
"""

from bottle import template


def main_page():
    """
    Main page handler

    :rtype: Template
    """
    return template('main.html')
