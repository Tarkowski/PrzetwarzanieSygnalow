"""
Handler for DFT
"""
from dft.bootstrap.dft import create_dft_service
from bottle import request, HTTPResponse
import json


class DftHandler:
    """
    Handler for DFT
    """
    @staticmethod
    def handle():
        """
        Handles count DFT request

        :rtype: {}
        """

        dft_service = create_dft_service()
        if not request.headers.get('X-Requested-With') == 'XMLHttpRequest':
            return HTTPResponse(status=400)

        sinusoid_samples = request.json['sinusoid_samples']
        response_data = dft_service.solve_dft(sinusoid_samples)

        return json.dumps(response_data)
