"""
Sinusoid handler
"""
import datetime
from sinusoid_signal.bootstrap.sinusoid import create_sinusoid_service
from sinusoid_signal.sinusoid_factory import TooLowSamplingFrequency
from sinusoid_signal.sinusoid_signal import DifferentSamplingFrequency
from bottle import request, HTTPResponse
import json


class SinusoidHandler:
    """
    Handler for generating sinusoid
    """

    def __init__(self):
        self._sinusoid_service = create_sinusoid_service()  # that should not be here

    def create_sinusoid(self):
        """
        Creates Sinusoid

        :rtype: HTTPResponse
        """
        if not request.headers.get('X-Requested-With') == 'XMLHttpRequest':
            return HTTPResponse(status=400)
        try:
            unpacked_values = self._create_data_dict(request.json)
        except ValueError:
            return HTTPResponse(status=401, body="Incorrect data")

        try:
            sinusoid = self._sinusoid_service.generate_sinusoid(
                unpacked_values["amplitude"],
                unpacked_values["frequency"],
                unpacked_values["sampling_frequency"],
                unpacked_values["phase_shift"],
                unpacked_values["duration"]
            )
        except TooLowSamplingFrequency:
            return HTTPResponse(status=400, body='Too low sampling frequency')

        return json.dumps(sinusoid)

    def add_sinusoid(self):
        """
        Creates Sinusoid

        :rtype: HTTPResponse
        """
        if not request.headers.get('X-Requested-With') == 'XMLHttpRequest':
            return HTTPResponse(status=400)
        try:
            unpacked_values = self._create_data_dict(request.json)
        except ValueError:
            return HTTPResponse(status=401, body="Incorrect data")

        try:
            sinusoid = self._sinusoid_service.add_sinusoid(
                unpacked_values["amplitude"],
                unpacked_values["frequency"],
                unpacked_values["sampling_frequency"],
                unpacked_values["phase_shift"],
                unpacked_values["duration"]
            )
        except (TooLowSamplingFrequency, DifferentSamplingFrequency, ValueError):
            return HTTPResponse(status=402, body='Too low sampling frequency or Different sampling frequency')

        return json.dumps(sinusoid)

    def sub_sinusoid(self):
        """
        Creates Sinusoid

        :rtype: HTTPResponse
        """
        if not request.headers.get('X-Requested-With') == 'XMLHttpRequest':
            return HTTPResponse(status=400)
        try:
            unpacked_values = self._create_data_dict(request.json)
        except ValueError:
            return HTTPResponse(status=401, body="Incorrect data")

        try:
            sinusoid = self._sinusoid_service.sub_sinusoid(
                unpacked_values["amplitude"],
                unpacked_values["frequency"],
                unpacked_values["sampling_frequency"],
                unpacked_values["phase_shift"],
                unpacked_values["duration"]
            )
        except (TooLowSamplingFrequency, DifferentSamplingFrequency, ValueError):
            return HTTPResponse(status=402, body='Too low sampling frequency or Different sampling frequency')

        return json.dumps(sinusoid)

    @staticmethod
    def _create_data_dict(request_data):
        """
        Creates dict with data from request

        :param dict request_data: Data for request

        :rtype: dict
        """
        unpacked_data = {}
        unpacked_data["amplitude"] = float(request_data['amplitude'])
        unpacked_data["frequency"] = float(request_data['frequency'])
        unpacked_data["sampling_frequency"] = float(request_data['sampling'])
        unpacked_data["phase_shift"] = float(request_data['shift'])
        duration_time = float(request_data['duration'])
        unpacked_data["duration"] = datetime.timedelta(seconds=duration_time)

        return unpacked_data
